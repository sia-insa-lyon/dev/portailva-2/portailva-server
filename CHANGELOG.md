# Portail VA 2 Back-end changelog

The latest version of this file can be found at the master branch of the Portail VA 2 Back-end repository.

## 0.1.0

### Removed (1 change)

- Remove unused dependencies

### Fixed (0 change, 0 of them are from the community)

### Changed (1 change)

- Revamp the project structure to have better code separation

### Added (6 changes)

- Adding CI/CD and environment configuration (Staging and Deployment)
- Adding issue and MR templates
- Adding Changelog, Contributing and env file
- Adding test to our app
- Adding docker-compose and helm charts to our app
- Adding SonarQube to our app

### Other (0 change)
