# Dockerfile
FROM php:8.0-fpm

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y libmcrypt-dev \
    build-essential \
    libpq-dev \
    libzip-dev \
    locales \
    zip \
    unzip \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_pgsql zip pcntl bcmath

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# install xdebug
RUN pecl install xdebug-3.1.0 && docker-php-ext-enable xdebug

# Install PHP Xdebug configuration
ADD ./docker-config/xdebug.ini /etc/php/conf.d/
ADD ./docker-config/xdebug.ini /usr/local/etc/php/conf.d/

# Copy existing application directory contents
COPY . /var/www

# Copy existing application directory permissions
RUN chown -R $USER:www-data storage bootstrap/cache && chmod -R 644 storage bootstrap/cache && chmod +x /var/www/docker-config/run-prod.sh

# Install dependencies
RUN composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts

# Expose port 9000(fpm)/9001(xDebug), run the migration and start php-fpm server
EXPOSE 9000 9001
CMD /var/www/docker-config/run-prod.sh
