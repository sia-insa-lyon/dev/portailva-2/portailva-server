<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssociationRequest;
use App\Http\Requests\AssociationUpdateRequest;
use App\Repositories\AssociationRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Ramsey\Uuid\Type\Integer;

/**
 * Class AssociationController linked with all the routes starting with '/association". This class implements CRUD
 * methods to handle the Association Model.
 *
 * @package App\Http\Controllers
 * @access public
 */
class AssociationController extends Controller
{
    private $associationRepository;

    /**
     * AssociationController constructor.
     *
     * @param AssociationRepository $associationRepository The Association Repository instance to inject in the
     * controller.
     */
    public function __construct(AssociationRepository $associationRepository)
    {
        $this->associationRepository = $associationRepository;
    }

    /**
     * Method returning a page of associations. This method takes an int as a parameter, indicating the page required.
     * If there is no page number passed, the default page returned will be page 0 (the first page).
     *
     * @param int $pageNum Page number required.
     * @return \Illuminate\Http\JsonResponse JSON with the page containing the associations.
     */
    public function getAllAssociations($pageNum = 0)
    {
        $associations = $this->associationRepository->getPaginate($pageNum);
        return response()->json($associations);
    }

    /**
     * Method taking an association's id as a parameter. Returns a JSON object with the association (status 200),
     * or an error with the right message and status code.
     *
     * @param int $id Id of the wanted association.
     * @return \Illuminate\Http\JsonResponse JSON Response of the method, either returning the requested association
     * or a message along with its error core.
     * @access public
     */
    public function getAssociation($id)
    {
        try {
            $association = $this->associationRepository->getById($id);
            return response()->json($association);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], $e->getCode());
        }
    }

    /**
     * Method used to create an association in the database. It will validate the fields of the request passed,
     * and insert the association in the database.
     *
     * @param AssociationRequest $request Request used to check if the passed body is correct and corresponds to an
     * association.
     * @return \Illuminate\Http\JsonResponse JSON containing the created association, or an error message along with its
     * status code.
     * @access public
     */
    public function createAssociation(AssociationRequest $request)
    {
        $validated = $request->validated();
        $created = $this->associationRepository->store($validated);
        return response()->json($created, 201);
    }

    /**
     * Method used to update the association whose id is passed as a parameter. The fields to update are passed in the
     * body of request. They will be in an array mapping the field's name to their value.
     *
     * @param AssociationUpdateRequest $request Request used to check if the passed body is correct.
     * @param $id Integer Id of the association to update.
     * @return \Illuminate\Http\JsonResponse JSON containing a message with number of associations that were updated, or
     * the error's message along with its status code.
     * @access public
     */
    public function updateAssociation(AssociationUpdateRequest $request, $id)
    {
        $validated = $request->validated();
        $updatedNumber = $this->associationRepository->update($id, $validated);
        return response()->json([
            'message' => $updatedNumber.' associations where updated'
        ]);
    }

    /**
     * Method used to delete an association whose id is passed as a parameter.
     *
     * @param $id Integer Id of the association to delete.
     * @return \Illuminate\Http\JsonResponse JSON containing a success message, or an error message along with its
     * status code.
     * @access public
     */
    public function deleteAssociation($id)
    {
        try {
            $this->associationRepository->destroy($id);
            return response()->json([
                'message' => 'The association was succesfully deleted'
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 404);
        }
    }
}
