<?php
namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

/**
* Middleware adding headers to our HTTP response
* @package  App\Http\Middleware
* @access   public
*/
class SecurityHeaders
{
    /**
     * Add more security to our HTTP headers
     *
     * @param Request $request
     * @param Closure $next
     * @return   mixed
     * @see https://owasp.org/www-project-secure-headers/
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        //TODO Enable CORS when we have the final production subdomain and URL : config('app')['url']
        $response->header('Access-Control-Allow-Origin', '*');

        return $response;
    }
}
