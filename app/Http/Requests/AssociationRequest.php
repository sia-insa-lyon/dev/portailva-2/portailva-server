<?php

namespace App\Http\Requests;

use App\Rules\Iban;
use App\Rules\Siren;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AssociationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TO-DO: change this function to check authentication
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'acronym' => 'string|max:255',
            'use_acronym' => 'boolean',
            'description' => 'string|max:255',
            'commentary' => 'string|max:255',
            'iban' => ['string', new Iban()],
            'bic' => ['digits:15'],
            'rna' => 'digits:10',
            'siren' => ['digits:9', new Siren()],
            'created_officially_at' => 'required|date_format:Y-m-d H:i:s',
            'active_members_number' => 'integer|gte:0',
            'all_members_number' =>  'integer|gte:0',
            'is_active' => 'required|boolean',
            'has_insa_status' => 'required|boolean'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'The association\'s name is required.',
            'created_officially_at.required' => 'The association\'s creation date is required.',
            'active_members_number.required' => 'The association\'s active members number is required.',
            'all_members_number.required' => 'The association\'s members number is required.',
            'has_insa_status.required' => 'It is required to know if the association has INSA status.'
        ];
    }
}
