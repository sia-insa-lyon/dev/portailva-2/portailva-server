<?php

namespace App\Http\Requests;

use App\Rules\Iban;
use App\Rules\Siren;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AssociationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TO-DO: change this function to check authentication
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255',
            'acronym' => 'string|max:255',
            'use_acronym' => 'boolean|max:255',
            'description' => 'string|max:255',
            'commentary' => 'string|max:255',
            'iban' => ['string', new Iban()],
            'bic' => ['digits:15'],
            'rna' => 'digits:10',
            'siren' => ['digits:9', new Siren()],
            'created_officially_at' => 'date_format:Y-m-d H:i:s',
            'active_members_number' => 'integer|gte:0',
            'all_members_number' => 'integer|gte:0',
            'is_active' => 'boolean',
            'has_insa_status' => 'boolean'
        ];
    }
}
