<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AssoRegistry representing the table "asso_registries" in the database. An asso registry is the page
 * visible on the website to the users. It contains many informations, such a description, or social network links.
 *
 * @package App\Models
 * @access public
 */
class AssoRegistry extends Model
{
    use HasFactory;

    protected $table = 'asso_registries';

    /**
     * Returns the list of entries the registry is linked with.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of AssoRegistryEntry
     */
    public function entries()
    {
        return $this->hasMany('App\Models\AssoRegistryEntry', 'registry');
    }

    /**
     * Returns the opening hour indicated on the registry. Each registry only has one opening hour.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne The opening hour.
     */
    public function openingHour()
    {
        return $this->hasOne('App\Models\OpeningHour');
    }
}
