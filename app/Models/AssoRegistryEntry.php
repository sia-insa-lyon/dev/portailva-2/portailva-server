<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AssoRegistryEntry representing the table "asso_registry_entry" in the database. An entry is different
 * from the registry.
 * Indeed, many associations may want to have their own page, but still be attached to an association.
 * To do so, they will have an entry linked to the asso registry they are linked with.
 *
 * @package App\Models
 * @access public
 */
class AssoRegistryEntry extends Model
{
    use HasFactory;

    protected $table = 'asso_registry_entries';

    /**
     * Returns the registry the entry is linked with.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The AssoRegistry.
     */
    public function registry()
    {
        return $this->belongsTo('App\Models\AssoRegistry');
    }
}
