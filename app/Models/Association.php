<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Association representing the association table in the database. It contains the main infos on an
 * association.
 *
 * @package App\Models
 * @access public
 */
class Association extends Model
{
    use HasFactory;

    protected $table = 'associations';

    // Make all fields mass assignable
    protected $guarded = [];

    /**
     * Returns all the users involved in the association.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany The list of users.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * Returns a list of all the relations with users the association is involved in.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of relations AssociationUser
     */
    public function associationUsers()
    {
        return $this->hasMany('App\Models\AssociationUser');
    }

    /**
     * Returns the user that moderates the association.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The moderating user.
     */
    public function moderatedBy()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Returns the category of the associations.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The association's category.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Returns all the equipments owned by this association.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of equipments.
     */
    public function equipmentsOwned()
    {
        return $this->hasMany('App\Models\Equipment', 'owner');
    }

    /**
     * Returns all reservations which the association is beneficiary.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of reservations.
     */
    public function reservationsBeneficiary()
    {
        return $this->hasMany('App\Models\Reservation', 'beneficiary');
    }
}
