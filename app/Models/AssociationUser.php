<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AssociationUser representing the pivot table between the tables "associations" and "users" in the database.
 * It is meant to represent the many to many relationship between a user and an association.
 * In the database, this many to many relationship will be represented with a pivot table, linking the user_id and
 * the association_id, hence the use of this class.
 *
 * @package App\Models
 * @access public
 */
class AssociationUser extends Model
{
    protected $table = 'association_user';

    /**
     * Returns the associated user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The user in the relation.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Returns the associated association
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The association in the relation.
     */
    public function association()
    {
        return $this->belongsTo('App\Association');
    }
}
