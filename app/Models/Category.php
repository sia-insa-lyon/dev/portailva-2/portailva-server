<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category representing the table "category" in the database. Categories are affiliated with associations.
 * An association has a category, and many categories may concern many associations.
 *
 * @package App\Models
 * @access public
 */
class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    /**
     * @var string[] The default values of the Category's attributes.
     */
    protected $attributes = [
        'color' => 'rgb(49,98,100,1.00)',
        'latex_color' => 'rgb(49,98,100,1.00)'
    ];

    /**
     * Returns the list of associations that belong to this category.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of associations.
     */
    public function associations()
    {
        return $this->hasMany('App\Models\Association', 'category');
    }
}
