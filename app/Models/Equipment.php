<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Equipment representing the equipment table in the database.
 * It contains the equipments of the résa app that can be reserved.
 *
 * @package App\Models
 * @access public
 */
class Equipment extends Model
{
    use HasFactory;

    protected $table = 'equipments';

    // Make all fields mass assignable
    protected $guarded = [];

    /**
     * Returns all the reservation about this equipment.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of reservation.
     */
    public function reservations()
    {
        return $this->hasMany('App\Models\Equipment', 'equipment');
    }

    /**
     * Returns the association of this equipment.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function associationEq()
    {
        return $this->belongsTo('App\Models\Association', 'owner');
    }

    /**
     * Returns the kind of this equipment.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kindEq()
    {
        return $this->belongsTo('App\Models\Kind', 'kind');
    }
}
