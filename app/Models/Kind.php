<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Kind representing the kind table in the database. It contains kinds of equipment of résa app.
 *
 * @package App\Models
 * @access public
 */
class Kind extends Model
{
    use HasFactory;

    protected $table = 'kinds';
    // Make all fields mass assignable
    protected $guarded = [];
    /**
     * Returns all the equipments with this kind.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of equipment.
     */
    public function equipments()
    {
        return $this->hasMany('App\Models\Equipment', 'kind');
    }
}
