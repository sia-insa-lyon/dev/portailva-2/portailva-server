<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OpeningHour representing the table "opening_hours" in the database. It describes the opening hours of
 * and association to be displayed on the website. It is linked with an AssoRegistry with a one to one
 * relationship.
 *
 * @package App\Models
 * @access public
 */
class OpeningHour extends Model
{
    use HasFactory;

    protected $table = 'opening_hours';

    /**
     * Returns the registry this opening hour is linked with.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo The registry.
     */
    public function registry()
    {
        return $this->belongsTo('App\Models\AssoRegistry', 'opening_hour');
    }
}
