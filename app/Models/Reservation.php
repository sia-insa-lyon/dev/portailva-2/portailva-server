<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reservation representing the table "reservations" in the database.
 * Reservations can be initiated by associations.
 * Each reservation is associated with one association and involves one equipment.
 *
 *
 * @package App\Models
 * @access public
 */
class Reservation extends Model
{
    use HasFactory;

    protected $table = 'reservations';

    // Make all fields mass assignable
    protected $guarded = [];

    /**
     * Returns the association beneficiary of this reservation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function associationBeneficiary()
    {
        return $this->belongsTo('App\Models\Association', 'beneficiary');
    }

    /**
     * Returns the equipment about this reservation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function equipmentResa()
    {
        return $this->belongsTo('App\Models\Equipment', 'equipment');
    }

    /**
     * Returns the user that commissioned this reservation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userBuyer()
    {
        return $this->belongsTo('App\Models\User', 'buyer');
    }

    /**
     * Returns the user that validates this reservation.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userValidator()
    {
        return $this->belongsTo('App\Models\User', 'validator');
    }
}
