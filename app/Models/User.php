<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User representing the table "user" in the database.  A this moment, a user can be a member of none or many
 *  associations, or moderate one or many associations.
 *
 * @package App\Models
 * @access public
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Returns all the associations the user belongs to.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany The associated associations.
     */
    public function associations()
    {
        return $this->belongsToMany('App\Models\Association');
    }

    /**
     * Returns a list of all the relations with associations the user is involved in.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of relations AssociationUser.
     */
    public function associationUsers()
    {
        return $this->hasMany('App\Models\AssociationUser');
    }

    /**
     * Returns all the associations the user moderates.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The associated associations.
     */
    public function associationsModerated()
    {
        return $this->hasMany('App\Models\Association', 'moderated_by');
    }

    /**
     * Returns all the reservations which the user is the validator
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of reservation.
     */
    public function reservationValidator()
    {
        return $this->hasMany('App\Models\Reservation', 'validator');
    }

    /**
     * Returns all the reservations which the user is the buyer
     * @return \Illuminate\Database\Eloquent\Relations\HasMany The list of reservation.
     */
    public function reservationBuyer()
    {
        return $this->hasMany('App\Models\Reservation', 'buyer');
    }
}
