<?php


namespace App\Providers;

use App\Models\User;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;
use KeycloakGuard\Exceptions\TokenException;
use KeycloakGuard\KeycloakGuard;
use KeycloakGuard\Token;

class KeycloakGuardRedone extends KeycloakGuard
{
    /**
     * This class overwrites the KeycloakGuard class given by the library robsontenorio/laravel-keycloak-guard
     * to circumvent the impossibility to create a user if they don't exist in db
     */

    private $config;
    private $user;
    private $provider;
    private $decodedToken;
    protected $request; // Changed from private for external access

    public function __construct(UserProvider $provider, Request $request)
    {
        parent::__construct($provider, $request);
        $this->config = config('keycloak');
        $this->user = null;
        $this->provider = $provider;
        $this->decodedToken = null;
        $this->request = $request;

        $this->authenticate();
    }

    /**
     * Had to redefine this function because they defined it private in the module...
     * @return mixed
     */
    private function authenticate(): void
    {
        try {
            $this->decodedToken = Token::decode($this->request->bearerToken(), $this->config['realm_public_key']);
        } catch (\Exception $e) {
            throw new TokenException($e->getMessage());
        }

        if ($this->decodedToken) {
            $this->validate([
                $this->config['user_provider_credential']
                    => $this->decodedToken->{$this->config['token_principal_attribute']}
            ]);
        }
    }

    /**
     * Given the token, logs in the user if they exist in the database,
     * create the db instance otherwise
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        if (!$this->decodedToken) {
            return false;
        }


        if ($this->config['load_user_from_database']) {
            $user = $this->provider->retrieveByCredentials($credentials);

            if (!$user) {
                /* This is the only part we change :
                    Create the user if they doesn't have an account */
                $user = new User;
                $user->name = $this->decodedToken->preferred_username;
                $user->email = $this->decodedToken->email;
                $user->save();
                /***********************************/
            }
        } else {
            $class = $this->provider->getModel();
            $user = new $class();
        }

        $this->setUser($user);

        return true;
    }
}
