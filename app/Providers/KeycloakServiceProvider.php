<?php


namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class KeycloakServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/keycloak.php' => app()->configPath('keycloak.php'),
        ], 'config');

        $this->mergeConfigFrom(__DIR__ . '/../../config/keycloak.php', 'keycloak');
    }

    public function register()
    {
        Auth::extend('keycloak_auth', function ($app, $name, array $config) {
            return new KeycloakGuardRedone(Auth::createUserProvider($config['provider']), $app->request);
        });
    }
}
