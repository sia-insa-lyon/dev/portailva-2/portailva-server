<?php
namespace App\Repositories;

use App\Models\Association;

class AssociationRepository extends ResourceRepository
{
    /**
     * AssociationRepository constructor.
     *
     * @param Association $associationModel The Association Model instance to inject in the repository.
     * @access public
     */
    public function __construct(Association $associationModel)
    {
        $this->model = $associationModel;
    }
}
