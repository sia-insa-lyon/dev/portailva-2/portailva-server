<?php
namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
* Abstract class to use common Eloquent method for every object
*
* @package  App\Repositories
* @access   public
*/
abstract class ResourceRepository
{

    /**
    * Model used for our methods
    *
    * @var Eloquent model
    */
    protected $model;

    /**
     * Method to get every object for this model using a paginated method
    * @param int $n                Int for pagination
    * @return Model|Collection     L'ensemble des modèles trouvées
     */
    public function getPaginate(int $n)
    {
        return $this->model->paginate($n);
    }

    /**
     * Method to store the model
     * @param  array      $inputs     Array with every property needed to build the model
     * @return Model                  Created model
     */
    public function store(Array $inputs)
    {
        return $this->model->firstOrCreate($inputs);
    }

    /**
     * Method to return a model corresponding to a specific id
     * @param  array|int        $id         ID, or array of ID for this model
     * @return Model|Collection             Every model associated to the ID provided
     * @throws ModelNotFoundException only if the model is not found
     */
    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * Method to update the information of a model
     * @param int $id       ID of the model to update
     * @param array $inputs Properties of the model to change
     * @return int          Number of model modified
     */
    public function update(int $id, array $inputs)
    {
        return $this->getById($id)->update($inputs);
    }

    /**
     * Method to remove the model from the database
     * @param   int $id              ID of the model to delete
     * @return  bool|null            Null if the model is not find, bool to indicate if the deletion has been done
     * @throws Exception only if we encounter an issue with the deletion
     */
    public function destroy(int $id)
    {
        return $this->getById($id)->delete();
    }
}
