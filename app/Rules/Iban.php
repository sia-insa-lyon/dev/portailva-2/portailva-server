<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class Iban that give a rule able to validate an IBAN string
 *
 * @package App\Rules
 * @access public
 * @see https://portal.hardis-group.com/pages/viewpage.action?pageId=120357227
 */
class Iban implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, mixed $value): bool
    {
        $value = str_replace(' ', '', $value);
        $frIbanRe = '^FR[0-9A-Z]{25}$^';
        if ((bool)preg_match($frIbanRe, $value) === false) {
            return false;
        }

        $verifyIban = str_split(substr($value, 4) . substr($value, 0, 4));
        $verifyIbanNum = '';
        foreach ($verifyIban as $c) {
            if ('A' <= $c && $c <= 'Z') {
                $c = (string)(ord($c) - ord('A') + 10);
            }
            $verifyIbanNum .= $c;
        }

        if ((int)bcmod($verifyIbanNum, '97') !== 1) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid IBAN.';
    }
}
