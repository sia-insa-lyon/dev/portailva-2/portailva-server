<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class Siren that give a rule able to validate a SIREN or SIRET number
 *
 * @package App\Rules
 * @access public
 * @see https://portal.hardis-group.com/pages/viewpage.action?pageId=120357227
 */
class Siren implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, mixed $value): bool
    {
        $sirenRe = '^[0-9]+$^';
        if ((bool)preg_match($sirenRe, $value) === false) {
            return false;
        }

        $sum = 0;
        $siren = str_split($value);
        for ($i = 0; $i < count($siren); $i++) {
            if (($i % 2) === 1) {
                $numTmp = (int)($siren[$i]) * 2;
                if ($numTmp > 9) {
                    $numTmp -= 9;
                }
            } else {
                $numTmp = (int)($siren[$i]);
            }
            $sum += $numTmp;
        }

        return (bool)(($sum % 10) === 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Siren number is invalid.';
    }
}
