<?php

namespace Database\Factories;

use App\Models\Association;
use App\Models\AssoRegistry;
use App\Models\AssoRegistryEntry;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssoRegistryEntryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AssoRegistryEntry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->paragraph(1),
            'description_en' => $this->faker->paragraph(1),
            'contact_address' => $this->faker->unique()->safeEmail,
            'phone' => $this->faker->phoneNumber,
            'website_url' => $this->faker->url,
            'facebook_url' => $this->faker->url,
            'twitter_url' => $this->faker->url,
            'discord_url' => $this->faker->url,
            'instagram_url' => $this->faker->url,
            'is_draft' => $this->faker->boolean,
            'is_online' => $this->faker->boolean,
            'registry' => AssoRegistry::factory(),
            'association' => Association::factory()
        ];
    }
}
