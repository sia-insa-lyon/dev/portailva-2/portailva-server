<?php

namespace Database\Factories;

use App\Models\Association;
use Illuminate\Database\Eloquent\Factories\Factory;

class AssociationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Association::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'acronym' => $this->faker->name,
            'use_acronym' => $this->faker->boolean,
            'description' => $this->faker->name,
            'commentary' => $this->faker->name,
            'bic' => $this->faker->numerify('###############'),
            'rna' => $this->faker->numerify('##########'),
            'siren' => "380438358",
            'iban' => $this->faker->iban('FR', '', 27),
            'created_officially_at' => $this->faker->dateTime->format('Y-m-d H:i:s'),
            'active_members_number' => $this->faker->randomDigitNotNull,
            'all_members_number' => $this->faker->randomDigitNotNull,
            'has_insa_status' => $this->faker->boolean,
            'is_active' => $this->faker->boolean,
        ];
    }
}
