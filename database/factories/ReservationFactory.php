<?php

namespace Database\Factories;

use App\Models\Reservation;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reservation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'quantity' => $this->faker->randomDigitNotNull,
            'motive' => $this->faker->name,
            'motive_refusal' => $this->faker->name,
            'statut' => $this->faker->randomDigitNotNull,
            'begins_at' => $this->faker->dateTime->format('Y-m-d H:i:s'),
            'ends_at' => $this->faker->dateTime->format('Y-m-d H:i:s')
        ];
    }
}
