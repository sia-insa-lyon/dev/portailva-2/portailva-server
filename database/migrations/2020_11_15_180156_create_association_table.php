<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssociationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('acronym')->nullable();
            $table->boolean('use_acronym')->nullable();
            $table->string('description')->nullable();
            $table->string('commentary')->nullable();
            $table->string('iban')->nullable();
            $table->string('bic')->nullable();
            $table->string('rna')->nullable();
            $table->string('siren')->nullable();
            $table->timestamp('created_officially_at')->nullable();
            $table->integer('active_members_number')->nullable();
            $table->integer('all_members_number')->nullable();
            $table->boolean('is_active');
            $table->boolean('has_insa_status');

            $table->unsignedBigInteger('moderated_by')->nullable();
            $table->foreign('moderated_by')
                ->references('id')
                ->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associations', function (Blueprint $table) {
            $table->dropForeign('associations_moderated_by_foreign');
        });
        Schema::dropIfExists('associations');
    }
}
