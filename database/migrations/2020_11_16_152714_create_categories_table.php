<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_en');
            $table->string('latex_color', 50);
            $table->string('color', 50);
            $table->timestamps();
        });

        Schema::table('associations', function (Blueprint $table) {
            $table->unsignedBigInteger('category')->nullable();
            $table->foreign('category')
                ->references('id')
                ->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associations', function (Blueprint $table) {
            $table->dropForeign('associations_category_foreign');
            $table->dropColumn('category');
        });
        Schema::dropIfExists('categories');
    }
}
