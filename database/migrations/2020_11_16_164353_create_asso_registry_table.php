<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssoRegistryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opening_hours', function (Blueprint $table) {
            $table->id();
            $table->integer('day');
            $table->timestamp('begins_at');
            $table->timestamp('ends_at');

            $table->timestamps();
        });

        Schema::create('asso_registries', function (Blueprint $table) {
            $table->id();
            $table->string('description')->nullable();
            $table->string('description_en')->nullable();
            $table->string('contact_address')->nullable();
            $table->string('phone')->nullable();
            $table->string('website_url')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('discord_url')->nullable();
            $table->boolean('is_draft');
            $table->boolean('is_online');

            $table->unsignedBigInteger('association');
            $table->foreign('association')
                ->references('id')
                ->on('associations');

            $table->unsignedBigInteger('opening_hour')->nullable();
            $table->foreign('opening_hour')
                ->references('id')
                ->on('opening_hours');

            $table->timestamps();
        });

        Schema::create('asso_registry_entries', function (Blueprint $table) {
            $table->id();
            $table->string('description')->nullable();
            $table->string('description_en')->nullable();
            $table->string('contact_address')->nullable();
            $table->string('phone')->nullable();
            $table->string('website_url')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('discord_url')->nullable();
            $table->boolean('is_draft');
            $table->boolean('is_online');

            $table->unsignedBigInteger('association');
            $table->foreign('association')
                ->references('id')
                ->on('associations');

            $table->unsignedBigInteger('registry');
            $table->foreign('registry')
                ->references('id')
                ->on('asso_registries');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asso_registry_entries', function (Blueprint $table) {
            $table->dropForeign('asso_registry_entries_association_foreign');
            $table->dropForeign('asso_registry_entries_registry_foreign');
        });
        Schema::dropIfExists('asso_registry_entries');

        Schema::table('asso_registries', function (Blueprint $table) {
            $table->dropForeign('asso_registries_association_foreign');
            $table->dropForeign('asso_registries_opening_hour_foreign');
        });
        Schema::dropIfExists('asso_registries');

        Schema::dropIfExists('opening_hours');
    }
}
