<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('description')->nullable();
            $table->string('description_en')->nullable();
            $table->integer('quantity');
            $table->integer('statut');
            $table->integer('guarantee');

            $table->unsignedBigInteger('kind')->nullable();
            $table->foreign('kind')
                ->references('id')
                ->on('kinds');

            $table->unsignedBigInteger('owner')->nullable();
            $table->foreign('owner')
                ->references('id')
                ->on('associations');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('associations', function (Blueprint $table) {
            $table->dropForeign('associations_owner_foreign');
        });
        Schema::table('kinds', function (Blueprint $table) {
            $table->dropForeign('kinds_kind_foreign');
        });
        Schema::dropIfExists('equipments');
    }
}
