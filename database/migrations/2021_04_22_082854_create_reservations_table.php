<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('equipment')->nullable();
            $table->foreign('equipment')
                ->references('id')
                ->on('equipments');

            $table->integer('quantity');

            $table->unsignedBigInteger('validator')->nullable();
            $table->foreign('validator')
                ->references('id')
                ->on('users');

            $table->unsignedBigInteger('buyer')->nullable();
            $table->foreign('buyer')
                ->references('id')
                ->on('users');

            $table->unsignedBigInteger('beneficiary')->nullable();
            $table->foreign('beneficiary')
                ->references('id')
                ->on('associations');

            $table->string('motive');
            $table->string('motive_refusal')->nullable();
            $table->integer('statut');
            $table->timestamp('begins_at');
            $table->timestamp('ends_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipments', function (Blueprint $table) {
            $table->dropForeign('equipments_equipment_foreign');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_validator_foreign');
            $table->dropForeign('users_buyer_foreign');
        });
        Schema::table('associations', function (Blueprint $table) {
            $table->dropForeign('associations_beneficiary_foreign');
        });
        Schema::dropIfExists('reservations');
    }
}
