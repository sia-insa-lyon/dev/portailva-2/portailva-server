<?php

use App\Http\Controllers\AssociationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//TODO To delete when another public end-point is added, left only to help new contributor to check if the api works
Route::get('/check', function (Request $request) {
    return response()->json(["message" => "API OK, Check end-point called"], 200);
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Group to prefix all routes with /association and redirect all incoming requests to the middleware to add header
Route::prefix('association')
    ->group(function () {
        // Route using GET method to get all the associations
        Route::get('/all/{pageNum?}', [AssociationController::class, 'getAllAssociations']);

        // Route using GET method to get an association whose id is passed as a route parameter
        Route::get('/{id}', [AssociationController::class, 'getAssociation']);

        // Route using POST method to create an association passed in the request's body
        Route::post('/', [AssociationController::class, 'createAssociation'])
            ->middleware('auth:api');

        // Route using PUT method to update an association whose id is passed as a route parameter, and the new content
        // is passed in the request's body
        Route::put('/{id}', [AssociationController::class, 'updateAssociation'])
            ->middleware('auth:api');

        // Route using DELETE method to delete an association whose id is passed as a route parameter
        Route::delete('/{id}', [AssociationController::class, 'deleteAssociation'])
            ->middleware('auth:api');
    });


// Useful for deployments and k8s healthcheck
Route::get('/check', function (Request $request) {
    return response()->json(["message" => "API OK, Check end-point called"], 200);
});

// For front-end testing purposes
Route::get('/check-auth', function (Request $request) {
    return response()->json(["message" => "API OK, user logged in"], 200);
}) ->middleware('auth:api');
