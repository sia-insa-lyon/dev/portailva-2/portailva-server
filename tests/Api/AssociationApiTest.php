<?php
namespace Tests\Api;

use App\Models\Association;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AssociationApiTest extends TestCase
{
    use RefreshDatabase;

    public function testCanBasicUserCreateAnAssociation()
    {
        // Create an association
        $association = Association::factory()->make();
        // Create a user to use the authentication
        $user = User::factory()->create();

        // Make a POST request and pass the association in the request's body
        $response = $this
            ->actingAs($user)
            ->postJson('/api/association', $association->toArray());
        // Get the id of the association
        $id = json_decode($response->getContent())->id;

        $response
            ->assertStatus(201)
            ->assertHeader('Content-Type', 'application/json');

        $this
            ->assertDatabaseCount('associations', 1)
            ->assertDatabaseHas('associations', [
                'id' => $id,
                'name' => $association->getAttribute('name'),
                'created_officially_at' => $association->getAttribute('created_officially_at'),
                'has_insa_status' => $association->getAttribute('has_insa_status'),
                'is_active' => $association->getAttribute('is_active'),
                'active_members_number' => $association->getAttribute('active_members_number'),
                'all_members_number' => $association->getAttribute('all_members_number')
            ]);

        // Test with non existing fields
        $failed = $this
            ->actingAs($user)
            ->postJson('/api/association', [
               'random' => 3,
               'fail' => null,
               'wrong' => 'hello'
            ]);
        $failed->assertStatus(422);

        $missingRequired = $this
            ->actingAs($user)
            ->postJson('/api/association', [
                'name' => 'Only a name is in the object'
            ]);
        $missingRequired->assertStatus(422);

        // Test with fields with wrong types
        $wrongValues = $this
            ->actingAs($user)
            ->postJson('/api/association', [
               'name' => 1,
               'has_insa_status' => 'true',
                'is_active' => 'false'
            ]);
        $wrongValues
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'has_insa_status',
                    'is_active',
                    'name'
                ]
            ]);
    }

    public function testCanBasicUserUpdateAnAssociation()
    {
        // Create an association
        $association = Association::factory()->make();
        // Create a user to use the authentication
        $user = User::factory()->create();

        // Make a POST request and pass the association in the request's body
        $created = $this
            ->actingAs($user)
            ->postJson('/api/association', $association->toArray());
        // Get the ID of the created association
        $id = json_decode($created->getContent())->id;

        // Update the association's name with a PUT request
        $response = $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, [
                'name' => 'MODIFIED'
            ]);

        $response
            ->assertStatus(200);
        $this->getJson('/api/association/'.$id)
            ->assertJson([
                'id' => $id,
                'name' => 'MODIFIED'
            ]);
    }

    public function testCanBasicUserDeleteAnAssociation()
    {
        // Create an association
        $association = Association::factory()->make();
        // Create a user to use the authentication
        $user = User::factory()->create();

        // Make a POST request and pass the association in the request's body
        $created = $this
            ->actingAs($user)
            ->postJson('/api/association', $association->toArray());
        // Get the ID of the created association
        $id = json_decode($created->getContent())->id;

        $this
            ->assertDatabaseCount('associations', 1)
            ->assertDatabaseHas('associations', [
                'id' => $id
            ]);

        // Send DELETE request
        $response = $this
            ->actingAs($user)
            ->deleteJson('/api/association/'.$id);
        $response->assertStatus(200);
        $this
            ->assertDatabaseCount('associations', 0)
            ->assertDatabaseMissing('associations', [
                'id' => $id
            ]);
    }

    public function testCanBasicUserGetAnAssociation()
    {
        // Create an association
        $association = Association::factory()->make();
        // Create a user to use the authentication
        $user = User::factory()->create();

        // Make a POST request and pass the association in the request's body
        $created = $this
            ->actingAs($user)
            ->postJson('/api/association', $association->toArray());
        // Get the ID of the created association
        $id = json_decode($created->getContent())->id;

        // Get the association with a GET request
        $response = $this->json('GET', '/api/association/'.$id);
        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => $id
            ]);
    }

    public function testCanBasicUserGetAllAssociations()
    {
        $response = $this->getJson('/api/association/all');
        $response->assertStatus(200);
        $this->assertDatabaseCount('associations', 0);
    }

    public function testAssociationIbanField()
    {
        // Create an association
        $association = Association::factory()->make();
        // Create a user to use the authentication
        $user = User::factory()->create();

        // Make a POST request and pass the association in the request's body
        $response = $this
            ->actingAs($user)
            ->postJson('/api/association', $association->toArray());
        // Get the id of the association
        $id = json_decode($response->getContent())->id;

        // Update the IBAN field
        $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, ['iban' => 'fezfez'])
            ->assertJsonStructure([
                'errors' => [
                    'iban'
                ]
            ]);

        //Test different invalid IBAN formats
        $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, ['iban' => 'FR12 1234 1234 1234 1234'])
            ->assertStatus(422);
        $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, ['iban' => 'FR12 1234 1234 1234 1234 1'])
            ->assertStatus(422);
        $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, ['iban' => 'FR12 1234 1234 1234 1234 12'])
            ->assertStatus(422);
        $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, ['iban' => 'FR123456789012345678'])
            ->assertStatus(422);
        $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, ['iban' => 'FR1234567890123456789'])
            ->assertStatus(422);
        $this
            ->actingAs($user)
            ->putJson('/api/association/'.$id, ['iban' => 'FR12345678901234567890'])
            ->assertStatus(422);
    }
}
