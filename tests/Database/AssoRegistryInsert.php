<?php

namespace Tests\Database;

use App\Models\AssoRegistry;
use App\Models\AssoRegistryEntry;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AssoRegistryInsert extends TestCase
{
    use RefreshDatabase;

    public function testInsertAssoRegistry()
    {
        // Create an asso_registry
        $registry = AssoRegistry::factory()->create();

        // Check if the asso_registry was correctly inserted
        $this->assertDatabaseHas('asso_registries', [
            'id' => $registry->getKey()
        ]);
    }

    public function testInsertAssoRegistryEntry()
    {
        // Create an asso_registy_entry
        $entry = AssoRegistryEntry::factory()->create();

        // Check if it was correctly inserted
        $this->assertDatabaseHas('asso_registry_entries', [
            'id' => $entry->getKey()
        ]);
    }
}
