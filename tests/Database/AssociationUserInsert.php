<?php

namespace Tests\Database;

use App\Models\Category;
use App\Models\User;
use Tests\TestCase;
use App\Models\Association;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AssociationUserInsert extends TestCase
{
    use RefreshDatabase;

    public function testInsertAssociation()
    {
        // Create an association
        $inserted = Association::factory()->create();

        // Create a user to add in the moderated_by field
        $user = User::factory()->create();
        $user->associationsModerated()->save($inserted);

        // Create a category and insert it in the category field of the association
        $category = Category::factory()->create();
        $category->associations()->save($inserted);

        // Check if the association is correctly inserted
        $this->assertDatabaseHas('associations', [
            'id' => $inserted->getKey(),
            'moderated_by' => $user->getKey(),
            'category' => $category->getKey()
        ]);
    }

    public function testInsertUser()
    {
        // Create a user and check if he was properly inserted
        $inserted = User::factory()->create();
        $this->assertDatabaseHas('users', [
            'id' => $inserted->getKey()
        ]);
    }

    public function testInsertAssociationUser()
    {
        // Populate users
        User::factory()->count(4)->create();

        // Populate associations
        Association::factory()->count(6)->create();

        // Get all associations to attach 2 random ones to each user
        $associations = Association::all();

        // Populate the association_user pivot table
        User::all()->each(function ($user) use ($associations) {
            $user->associations()->attach(
                $associations->random(2)->pluck('id')->toArray()
            );
        });

        // Check the number of inserted elements in each database
        $this->assertDatabaseCount('users', 4);
        $this->assertDatabaseCount('associations', 6);
        $this->assertDatabaseCount('association_user', 8);
    }
}
