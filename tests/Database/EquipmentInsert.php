<?php

namespace Tests\Database;

use App\Models\Equipment;
//use App\Models\Reservation;
use App\Models\Kind;
use App\Models\Association;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EquipmentInsert extends TestCase
{
    use RefreshDatabase;

    public function testInsertEquipment()
    {
        // Create an equipment
        $inserted = Equipment::factory()->create();

        // Create a kind and insert it in the kind field of the equipment
        $kind = Kind::factory()->create();
        $kind->equipments()->save($inserted);

        // Create a owner association and insert it in the owner field of the equipment
        $owner = Association::factory()->create();
        $owner->equipmentsOwned()->save($inserted);


        // Check if the equipment was correctly inserted
        $this->assertDatabaseHas('equipments', [
            'id' => $inserted->getKey(),
            'kind' => $kind->getKey(),
            'owner' => $owner->getKey()
        ]);
    }

    public function testInsertKindEquipment()
    {
        // Create an equipment
        $equipment = Equipment::factory()->create();
        $asso = Association::factory()->create();

        $equipment->associationEq()->associate($asso);
        $equipment->save();

        // Check if the kind was correctly associated with the equipment
        $this->assertDatabaseHas('equipments', [
            'id' => $equipment->getKey(),
            'owner' => $asso->getKey()
        ]);
    }

}
