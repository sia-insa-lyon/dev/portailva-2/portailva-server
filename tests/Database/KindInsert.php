<?php

namespace Tests\Database;

use App\Models\Equipment;
use App\Models\Kind;
//use App\Models\Equipment;
//use App\Models\Place;
//use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class KindInsert extends TestCase
{
    use RefreshDatabase;

    public function testInsertKind()
    {
        // Create an kind
        $inserted = Kind::factory()->create();
        // Check if the kind was correctly inserted
        $this->assertDatabaseHas('kinds', [
            'id' => $inserted->getKey()
        ]);
    }

    public function testInsertKindEquipment()
    {
        // Create an equipment
        $equipment = Equipment::factory()->create();
        $kind = Kind::factory()->create();

        $equipment->kindEq()->associate($kind);
        $equipment->save();

        // Check if the kind was correctly associated with the equipment
        $this->assertDatabaseHas('equipments', [
            'id' => $equipment->getKey(),
            'kind' => $kind->getKey()
        ]);
    }

}
