<?php

namespace Tests\Database;

use App\Models\Reservation;
use App\Models\Equipment;
use App\Models\Association;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReservationInsert extends TestCase
{
    use RefreshDatabase;

    public function testInsertReservation()
    {
        // Create a reservation
        $inserted = Reservation::factory()->create();


        // Create a equipment and insert it in the equipment field of the reservation
        $equipment = Equipment::factory()->create();
        $equipment->reservations()->save($inserted);


        // Create a user and insert in the validator field of the reservation
        $validator = User::factory()->create();
        $validator->reservationValidator()->save($inserted);


        // Create a user and insert in the buyer field of the reservation
        $buyer = User::factory()->create();
        $buyer->reservationBuyer()->save($inserted);


        // Create the beneficiary association and insert it in the beneficiary field of the reservation
        $beneficiary = Association::factory()->create();
        $beneficiary->reservationsBeneficiary()->save($inserted);


        // Check if the equipment was correctly inserted
        $this->assertDatabaseHas('reservations', [
            'id' => $inserted->getKey(),
            'equipment' => $equipment->getKey(),
            'validator' => $validator->getKey(),
            'buyer' => $buyer->getKey()
        ]);
    }

    public function testInsertAttributesReservation()
    {
        // Create an equipment
        $reservation = Reservation::factory()->create();
        $beneficiary = Association::factory()->create();
        $equipment = Equipment::factory()->create();
        $buyer = User::factory()->create();
        $validator = User::factory()->create();

        $reservation->associationBeneficiary()->associate($beneficiary);
        $reservation->equipmentResa()->associate($equipment);
        $reservation->userBuyer()->associate($buyer);
        $reservation->userValidator()->associate($validator);
        $reservation->save();

        // Check if the kind was correctly associated with the equipment
        $this->assertDatabaseHas('reservations', [
            'id' => $equipment->getKey(),
            'beneficiary' => $beneficiary->getKey(),
            'equipment' => $equipment->getKey(),
            'buyer' => $buyer->getKey(),
            'validator' => $validator->getKey(),
        ]);
    }

}
