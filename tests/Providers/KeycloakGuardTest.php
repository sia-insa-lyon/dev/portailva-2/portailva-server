<?php
namespace Tests\Providers;

use Firebase\JWT\JWT;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class KeycloakGuardTest extends TestCase
{
    /**
     * Only test the functions we rewrite from the original library
     */

    use RefreshDatabase;

    /**
     * @see https://github.com/robsontenorio/laravel-keycloak-guard/
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->privateKey = openssl_pkey_new(array(
            'digest_alg' => 'sha256',
            'private_key_bits' => 1024,
            'private_key_type' => OPENSSL_KEYTYPE_RSA
        ));

        $this->publicKey = openssl_pkey_get_details($this->privateKey)['key'];

        $this->payload = [
            'preferred_username' => 'johndoe',
            'resource_access' => ['account' => []]
        ];

        $this->token = JWT::encode($this->payload, $this->privateKey, 'RS256');

        // Set Keycloak Guard default configs
        config(['keycloak.realm_public_key' => $this->plainPublicKey($this->publicKey)]);
    }

    public function testForbidsAccessLoggedOut()
    {
        // We verify if logged out users still can't access locked pages
        $response = $this->getJson('/api/check-auth');
        $response->assertStatus(401);
        $this->assertGuest();
        $response->assertExactJson(array("message"=>"Unauthenticated."));
    }

    public function testCreatesUserObject()
    {
        $this->buildCustomToken([
            'preferred_username' => 'coco',
            'email' => 'coco@test.com'
        ]);

        $response = $this->withAuthToken()->getJson('/api/check-auth');

        $response->assertStatus(200);
        $this->assertAuthenticated();

        $this
            ->assertDatabaseCount('users', 1)
            ->assertDatabaseHas('users', [
                'name'=> 'coco',
                'email'=> 'coco@test.com'
            ]);
    }

    protected function plainPublicKey($key)
    {
        $string = str_replace('-----BEGIN PUBLIC KEY-----', '', $key);
        $string = trim(str_replace('-----END PUBLIC KEY-----', '', $string));
        $string = str_replace('\n', '', $string);

        return $string;
    }

    protected function buildCustomToken(array $payload)
    {
        $payload = array_replace($this->payload, $payload);
        $this->token = JWT::encode($payload, $this->privateKey, 'RS256');
    }

    protected function withAuthToken()
    {
        $this->withHeaders(['Authorization' => 'Bearer ' . $this->token]);

        return $this;
    }
}
