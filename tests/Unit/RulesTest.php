<?php

namespace Tests\Unit;

use App\Rules\Iban;
use App\Rules\Siren;
use PHPUnit\Framework\TestCase;

class RulesTest extends TestCase
{
    /**
     * Check that correct Iban is validated
     *
     * @return void
     */
    public function testCanValidateCorrectIban()
    {
        $ibanRule = new Iban();
        //See https://www.iban.fr/exemple.html
        $this->assertTrue($ibanRule->passes('', 'FR7630001007941234567890185'));
        $this->assertTrue($ibanRule->passes('', 'FR7630004000031234567890143'));
        $this->assertTrue($ibanRule->passes('', 'FR7630006000011234567890189'));
        $this->assertTrue($ibanRule->passes('', 'FR7610107001011234567890129'));
    }

    /**
     * Check that Iban with space is validated
     *
     * @return void
     */
    public function testCanValidateIbanWithSpace()
    {
        $ibanRule = new Iban();
        $this->assertTrue($ibanRule->passes('', 'FR76 3000 1007 9412 3456 7890 185'));
        $this->assertTrue($ibanRule->passes('', 'FR7630 00400 003 12345 678 90143'));
        $this->assertTrue($ibanRule->passes('', 'F R7630 0060 000 112345678 90189'));
        $this->assertTrue($ibanRule->passes('', 'FR 761 0107001011 23456789 0129'));
        $this->assertTrue($ibanRule->passes('', 'FR7610107001011234567890129'));
    }

    /**
     * Check that incorrect Iban is not validated
     *
     * @return void
     */
    public function testCanInvalidateWrongIban()
    {
        $ibanRule = new Iban();
        //Wrong IBAN number
        $this->assertFalse($ibanRule->passes('', 'FR7630001807941234567890185'));
        //Wrong IBAN length
        $this->assertFalse($ibanRule->passes('', 'FR763000031234567890143'));
        $this->assertFalse($ibanRule->passes('', '763000600011234567890189'));
        $this->assertFalse($ibanRule->passes('', 'FR761010700101125334567890129'));
        //Wrong IBAN locator or numeric char
        $this->assertFalse($ibanRule->passes('', 'DE7630006000011234567890189'));
        $this->assertFalse($ibanRule->passes('', 'FRA630006000011234567890189'));
    }

    /**
     * Check that correct Siren is validated
     *
     * @return void
     */
    public function testCanValidateCorrectSiren()
    {
        $sirenRule = new Siren();
        $this->assertTrue($sirenRule->passes('', '380438358'));
        $this->assertTrue($sirenRule->passes('', '430370965'));
        $this->assertTrue($sirenRule->passes('', '814603023'));
        $this->assertTrue($sirenRule->passes('', '847816550'));
        $this->assertTrue($sirenRule->passes('', '838662435'));
    }

    /**
     * Check that incorrect Siren is not validated
     *
     * @return void
     */
    public function testCanInvalidateWrongSiren()
    {
        $sirenRule = new Siren();
        //Wrong SIREN value
        $this->assertFalse($sirenRule->passes('', '381438358'));
        //Wrong SIREN length
        $this->assertFalse($sirenRule->passes('', '38048358'));
        $this->assertFalse($sirenRule->passes('', '3820438358'));
        $this->assertFalse($sirenRule->passes('', '3838358'));
        //Wrong SIREN content
        $this->assertFalse($sirenRule->passes('', '040000000'));
        $this->assertFalse($sirenRule->passes('', '0Ac500000'));
    }
}
